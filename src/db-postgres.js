'use strict';

var Promise = require('promise');
var pg = require('pg');
var moment = require('moment');

const initialize = connectionString => {
  return new Promise((resolve, reject) => {
    pg.connect(connectionString, (err, client, done) => {
      if (err) {
        done();
        return reject();
      }

      const CREATE_QUERY = `CREATE TABLE IF NOT EXISTS events(
        ID             INT PRIMARY KEY     NOT NULL,
        NAME           TEXT    NOT NULL,
        DESCRIPTION    TEXT,
        STARTDATE      TIMESTAMP,
        ENDDATE        TIMESTAMP,
        IN_TIMESTAMP   TIMESTAMP,
        IS_SYNCED      BOOLEAN)`;
      var query = client.query(CREATE_QUERY);
      query.on('end', () => {
        done();
        return resolve();
      });
    });
  });
};

const putItem = connectionString => id => {

  return new Promise((resolve, reject) => {
    pg.connect(connectionString, (err, client, done) => {
      if (err) {
        done();
        return reject();
      }
      
      var query = client.query(
        'INSERT INTO events VALUES($1, $2, $3, $4, $5, now(), $6)', 
        [ 1,  'test event name', 'test descriptions', '2018-10-8 15:36:38', '2018-10-16 15:36:38', false]);

      query.on('end', () => {
        done();
        return resolve();
      });
    });
  });
}

const getItem = connectionString => id => {
  return new Promise((resolve, reject) => {
    pg.connect(connectionString, (err, client, done) => {
      if (err) {
        done();
        return reject();
      }

      var results = [];
      var query = client.query('SELECT * FROM events WHERE id=($1)', [id]);

      query.on('row', (row) => {
        results.push(row);
      });

      query.on('end', () => {
        done();
        return resolve(results.length > 0 ? results[0] : null);
      });
    });
  });
}

export default function (connectionString) {

  return initialize(connectionString).then(() => {
    return {
      put: putItem(connectionString),
      get: getItem(connectionString)
    }
  });

}
